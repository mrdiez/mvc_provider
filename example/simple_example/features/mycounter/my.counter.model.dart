import '../../../../lib/mvcprovider.dart';
import 'my.counter.ctrl.dart';

class MyCounterModel extends MVC_Model<MyCounterCtrl> {
  int _count = 0;
  set count(int value) {
    _count = value;
    notifyListeners();
  }

  int get count => _count;
}
